#include<stdio.h>

int main ()
{
    FILE *fIn, *fOut;
	int choice,count=1;

	char id[' '], name[' '], semester[' '];

	fIn = fopen("sp-practical-1.txt", "r");
	if (fIn == NULL)
	{
        	printf("\nFailed to open file.");
        	return 1;
    }

	fOut = fopen("sp-practical-1.txt", "a");

	do
	{
		printf("\n1. READ RECORDS");
		printf("\n2. WRITE RECORDS");
		printf("\n3. EXIT");
		printf("\nEnter your Choice : ");
		scanf("%d",&choice);
		switch(choice)
		{
			case 1 : fseek(fIn,0,SEEK_SET);
					 count=1; 
					 while(!feof(fIn))
					 {
					 	 printf("\nSTUDENT %d\n",count);
					 	 fscanf(fIn,"%s",id);
					 	 printf("ID : %s\n",id);
					 	 fscanf(fIn,"%s",name);
					 	 printf("Name : %s\n",name);
					 	 fscanf(fIn,"%s\n",semester);
					 	 printf("Semester : %s\n",semester);
					 	 count++;
					 }
					 break;

			case 2 : fflush(stdin);
					 printf("Enter ID : ");
					 scanf("%s",id);
					 printf("Enter Name : ");
					 scanf("%s",name);
					 printf("Enter Semester : ");
					 scanf("%s",semester);
					 fprintf(fOut, "%s %s %s\n", id, name, semester);
					 break;
			case 3 : break;
		}
	}while(choice != 3);
	return 0;
}
