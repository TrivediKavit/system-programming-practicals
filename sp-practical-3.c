#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int size=0;
void Insert();
void Display();
void Delete();
int Search(char[]);
void Modify();
struct SymbTab
{
  char label[10],type[10];
  int addr;
  struct SymbTab *next;
};

struct SymbTab *first,*last;

int main()
{
  int choice,flag;
  char label[10];
  do
  {
    printf("\n1.INSERT\n2.DISPLAY\n3.DELETE\n4.SEARCH\n5.MODIFY\n6.EXIT\n");
    printf("\nEnter your choice : ");
    scanf("%d",&choice);
    switch(choice)
    {
     case 1:
        Insert();
        break;
     case 2:
        Display();
        break;
     case 3:
        Delete();
        break;
     case 4:
        printf("\n\tEnter the label to be searched : ");
        scanf("%s",label);
        flag=Search(label);
        printf("\n\tSearch Result:");
        if(flag==1)
          printf("\n\tThe label is present in the symbol table\n");
        else
          printf("\n\tThe label is not present in the symbol table\n");
        break;
     case 5:
        Modify();
        break;
     case 6:
        break;
    }
  }while(choice != 6);
  return 0;
}

void Insert()
{
  int flag;
  char label[10];
  printf("\nEnter the label : ");
  scanf("%s",label);
  flag=Search(label);
  if(flag==1)
  {
    printf("\n\tThe label exists already in the symbol table\n\tDuplicate can't be inserted");
  }
  else
  {
    struct SymbTab *p;
    p=malloc(sizeof(struct SymbTab));
    strcpy(p->label,label);
    printf("\nEnter the type : ");
    scanf("%s",p->type);
    printf("\nEnter the address : ");
    scanf("%d",&p->addr);
    p->next=NULL;
    if(size==0)
    {
      first=p;
      last=p;
    }
    else
    {
      last->next=p;
      last=p;
    }
    size++;
  }
  printf("\n\tLabel Inserted\n");
}

void Display()
{
  int i;
  struct SymbTab *p;
  p=first;
  printf("\n\tLABEL\t\tTYPE\t\tADDRESS\n");
  for(i=0;i<size;i++)
  {
    printf("\t%s\t\t%s\t\t%d\n",p->label,p->type,p->addr);
    p=p->next;
  }
}

int Search(char label[])
{
  int i,flag=0;
  struct SymbTab *p;
  p=first;
  for(i=0;i<size;i++)
  {
    if(strcmp(p->label,label)==0)
    {
      flag=1;
      break;
    }
    p=p->next;
  }
  return flag;
}

void Modify()
{
  char l[10],nl[10];
  int addr,choice,i,flag;
  struct SymbTab *p;
  p=first;
  printf("\nWhat do you want to modify?\n");
  printf("\n1.Only the label\n2.Only the address\n3.Both\n");
  printf("\tEnter your choice : ");
  scanf("%d",&choice);
  switch(choice)
  {
    case 1: printf("\nEnter the old label : ");
            scanf("%s",l);
            flag=Search(l);
            if(flag==0)
              printf("\n\tLabel not found\n");
            else
            {
              printf("\nEnter the new label : ");
              scanf("%s",nl);
              for(i=0;i<size;i++)
              {
                if(strcmp(p->label,l)==0)
                  strcpy(p->label,nl);
                p=p->next;
              }
              printf("\nAfter Modification:\n");
              Display();
            }
            break;

    case 2: printf("\nEnter the label where the address is to be modified : ");
            scanf("%s",l);
            flag=Search(l);
            if(flag==0)
            {
              printf("\n\tLabel not found\n");
            }
            else
            {
              printf("\nEnter the new address : ");
              scanf("%d",&addr);
              for(i=0;i<size;i++)
              {
                if(strcmp(p->label,l)==0)
                p->addr=addr;
                p=p->next;
              }
              printf("\nAfter Modification:\n");
              Display();
            }
            break;

    case 3: printf("\nEnter the old label : ");
            scanf("%s",l);
            flag=Search(l);
            if(flag==0)
            {
              printf("\n\tLabel not found\n");
            }
            else
            {
              printf("\nEnter the new label : ");
              scanf("%s",nl);
              printf("\nEnter the new address : ");
              scanf("%d",&addr);
              for(i=0;i<size;i++)
              {
                if(strcmp(p->label,l)==0)
                {
                  strcpy(p->label,nl);
                  p->addr=addr;
                }
                p=p->next;
              }
              printf("\nAfter Modification:\n");
              Display();
            }
            break;
  }
}

void Delete()
{
  int flag;
  char label[10];
  struct SymbTab *p,*q;
  p=first;
  printf("\nEnter the label to be deleted : ");
  scanf("%s",label);
  flag=Search(label);
  if(flag==0)
  {
    printf("\n\tLabel not found\n");
  }
  else
  {
    if(strcmp(first->label,label)==0)
    {
      first=first->next;
    }
    else if(strcmp(last->label,label)==0)
    {
      q=p->next;
      while(strcmp(q->label,label)!=0)
      {
        p=p->next;
        q=q->next;
      }
      p->next=NULL;
      last=p;
    }
    else
    {
      q=p->next;
      while(strcmp(q->label,label)!=0)
      {
        p=p->next;
        q=q->next;
      }
      p->next=q->next;
    }
    size--;
    printf("\n\tAfter Deletion:\n");
    Display()
  }
}