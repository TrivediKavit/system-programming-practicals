#include<stdio.h>

int main(int argc, char* argv[])
{
	char c;
	FILE *fIn,*fOut;
	if(argc<3)
	{
		printf("Usage: ./sp-practical-2.out <source-file> <destination-file>\n");
		return 1;
	}
	else
	{
		printf("Copying ...\n");
		printf("Source File: %s\n",argv[1]);
		printf("Destination File: %s\n",argv[2]);
		fIn = fopen(argv[1],"r");
		if (fIn == NULL)
		{
        	printf("\nFailed to open file.");
        	return 1;
    	}
		fOut = fopen(argv[2],"w");
	}

	while(!feof(fIn))
	{
		c = fgetc(fIn);
		fputc(c,fOut);
	}

	printf("File Copied ...\n");
	return 0;
}
