#include<stdio.h>
#include<string.h>
#include<ctype.h>

char *operators[]={ "<", ">", ";", "(", ")", "{", "}", "=", "\"", NULL };
char *keywords[]={ "int", "include", "stdio.h", "main", "printf", "return", "define", NULL };
char *preprocessor[]={"#"};
int i,flag;

int check(char *arg,int condition);
int isFound(char *arg, char *array[]);
void lexIT(char *);

int main(int argc,char *argv[])
{
	FILE *fIn;

	char word[' '];
	fIn=fopen("dummy.c","r");

	while(!feof(fIn))
	{
		fscanf(fIn,"%s",word);
		lexIT(word);
	}
	
	return 0;
}

int isFound(char *arg, char *array[])
{
	i=0;
	while(array[i] != NULL)
	{
		if(strcmp(arg,array[i]) == 0)
		{
			return 1;
		}
		i++;
	}
	return 0;
}

int check(char *arg,int condition)
{
	if(condition == 0)
	{
		for(i=0; i<strlen(arg); i++)
		{
			if(!isdigit(arg[i]))
			{
				return 1;
			}
		}
		return 0;
	}
	else if(condition == 1)
	{
		for(i=0; i<strlen(arg); i++)
		{
			if(!isalnum(arg[i]))
			{
				return 1;
			}
		}
		return 0;
	}
	else 
	{
		return 2;
	}
}

void lexIT(char *arg)
{
	flag=0;

	if(isFound(arg,preprocessor) && !flag)
	{
		printf("%s is an Preprocessor Directive.\n", arg);
		flag=1;
	}

	if(isFound(arg,operators) && !flag)
	{
		printf("%s is an Operator.\n", arg);
		flag=1;
	}

	if((isFound(arg,keywords)) && !flag)
	{
		printf("%s is a Keyword.\n", arg);
		flag=1;
	}

	if((check(arg,0) == 0) && !flag)
	{
		printf("%s is a Digit.\n", arg);
		flag=1;
	}

	if((check(arg,1) == 0) && !flag)
	{
		printf("%s is an Identifier.\n", arg);
		flag=1;
	}

	if(!flag)
	{
		printf("%s is a Literal.\n", arg);
	}

}